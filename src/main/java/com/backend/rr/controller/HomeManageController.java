package com.backend.rr.controller;

import com.backend.rr.entity.*;
import com.backend.rr.repository.*;
import com.backend.rr.service.BooksService;
import com.backend.rr.service.DetailHomeService;
import com.backend.rr.service.HomeService;
import com.backend.rr.service.ProfileService;
import flexjson.JSONSerializer;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.sound.sampled.Line;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RestController
public class HomeManageController {
    //Service = get only  / Repository add delete update
    @Autowired
    DetailHomeService detailHomeService;

    @Autowired
    HomeAddressRepository homeAddressRepository;

    @Autowired
    HomeRepository homeRepository;

    @Autowired
    BooksService booksService;

    @Autowired
    BooksRepository booksRepository;

    @Autowired
    ProfileService profileService;

    @Autowired
    InformRepository informRepository;

    @Autowired
    HomeService homeService;

    @Autowired
    ContractRepository contractRepository;
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-09-16
    @Update Date  2019-09-17
    * method : getDetailHome()
    * Manage : Get ข้อมูลจาก Service
    */
    @GetMapping("/getDetailHome")
    public ResponseEntity<String> getDetailHome(){
        HttpHeaders headers = new HttpHeaders();
        headers.add("statusValidate", "-1");
        try {
            return new ResponseEntity<String>((new JSONSerializer().exclude("*.class").deepSerialize(detailHomeService.getDetailHome())), headers, HttpStatus.OK);
        } catch (Exception e) {
            headers.add("statusValidate","1");
            return new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-09-16
    @Update Date  2019-09-17
    * method : getReservDetailHome(String id_home)
    * Manage : Get ข้อมูลจาก Service
    */
    @RequestMapping(value = "/getReservDetailHome", method = RequestMethod.GET, produces = "text/html;charset=utf-8", headers = "Accept=application/json")
    public ResponseEntity<String> getReservDetailHome(@RequestParam(name = "id") String id_home) {
        Home home = detailHomeService.getReservDetailHome(id_home);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        try {
            return new ResponseEntity<String>((new JSONSerializer().exclude("*.class").deepSerialize(home)), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            headers.add("StatusValidate", "1");
            return new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-09-16
    @Update Date  2019-09-17
    * method : getListBooks()
    * Manage : Get ข้อมูลจาก Service
    */
    @GetMapping("/getListBooks")
    public List<Books> getListBooks() {
        return booksService.getListBooks();
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-09-25
    @Update Date  -
    * method : addBooks(String json)
    * Manage : Map Key ที่ Post มาแล้วทำการ Set ค่า
    */
    @RequestMapping(value = "/addBooks", method = RequestMethod.POST, produces = "text/html;charset=utf-8", headers = "Accept=application/json")
    public ResponseEntity<String> addBooks(@RequestBody String json) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        try {
            JSONObject jsonObject = new JSONObject(json);
            String date_book = jsonObject.getString("date_book");
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(date_book);
            String bill_book = jsonObject.getString("bill_book");//getString คือ key
            String status_book = jsonObject.getString("status_book");
            int statusBook = Integer.valueOf(status_book);
            // Get ข้อมูล Home ที่ id_home
            String id_home = jsonObject.getString("id_home");
            Home home = detailHomeService.getReservDetailHome(id_home);
            // Get ข้อมูล user ที่ id_user
            String id_user = jsonObject.getString("id_user");
            User user = profileService.getEditProfile(id_user);

            home.setStatus_home(2);

            Inform inform = new Inform();
            inform.setTopic("1");
            inform.setDesciption("Reserve a house");
            inform.setUser(user);
            inform.setDate_submit(date);
            inform.setStatus_inform(1);
            inform.setHome(home);
            informRepository.saveAndFlush(inform);

            Books books = new Books();
            // set data books
            books.setDate_book(date);
            books.setBill_book(bill_book);
            books.setStatus_book(statusBook);
            books.setHome(home);
            books.setUser(user);
            booksRepository.saveAndFlush(books);
            return new ResponseEntity<String>(headers,HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            headers.add("StatusValidate", "1");
            return new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-09-25
    @Update Date  -
    * method : probInform(String json)
    * Manage : Map Key ที่ Post มาแล้วทำการ Set ค่า
    */
    @RequestMapping(value = "/probInform", method = RequestMethod.POST, produces = "text/html;charset=utf-8", headers = "Accept=application/json")
    public ResponseEntity<String> probInform(@RequestBody String json) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        try {
            JSONObject jsonObject = new JSONObject(json);
            String topic = jsonObject.getString("topic");
            String desciption = jsonObject.getString("desciption");
            String date = jsonObject.getString("date_submit");
            Date date_submit = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            String status = jsonObject.getString("status_inform");
            int status_inform = Integer.valueOf(status);

            String id_home = jsonObject.getString("id_home");
            Home home = detailHomeService.getReservDetailHome(id_home);

            String id_user = jsonObject.getString("id_user");
            User user = profileService.getEditProfile(id_user);

            Inform inform = new Inform();
            inform.setTopic(topic);
            inform.setDesciption(desciption);
            inform.setDate_submit(date_submit);
            inform.setStatus_inform(status_inform);
            inform.setHome(home);
            inform.setUser(user);
            informRepository.saveAndFlush(inform);
            return new ResponseEntity<String>(headers,HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            headers.add("StatusValidate", "1");
            return new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-09-30
    @Update Date  -
    * method : getHomeBooks(String id_books)
    * Manage : Get ข้อมูลจาก Service
    */
    @RequestMapping(value = "/getHomeBooks", method = RequestMethod.GET, produces = "text/html;charset=utf-8", headers = "Accept=application/json")
    public ResponseEntity<String> getHomeBooks(@RequestParam(name = "id") String id_books) {
        Books books = booksService.UpdateBooks(id_books);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        try {
            return new ResponseEntity<String>((new JSONSerializer().exclude("*.class").deepSerialize(books)), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            headers.add("StatusValidate", "1");
            return new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-10-1
    @Update Date  -
    * method : updateHomeBooks(String json)
    * Manage : Map Key ที่ Post มาแล้วทำการ Set ค่า
    */
    @RequestMapping(value = "/updateHomeBooks", method = RequestMethod.POST, produces = "text/html;charset=utf-8", headers = "Accept=application/json")
    public ResponseEntity<String> updateHomeBooks(@RequestBody String json) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        try {
            JSONObject jsonObject = new JSONObject(json);
            String id_books = jsonObject.getString("id_books");
            String today  = jsonObject.getString("today");
            Date date_submit = new SimpleDateFormat("yyyy-MM-dd").parse(today);
            // Get ข้อมูล user ที่ id_user
            String id_user = jsonObject.getString("id_user");
            User user = profileService.getEditProfile(id_user);

            Books books = booksService.UpdateBooks(id_books);
            booksService.UpdateBooks(id_books);
            books.setStatus_book(0);
            booksRepository.saveAndFlush(books);

            Home home = books.getHome();
            home.setStatus_home(1);
            homeRepository.saveAndFlush(home);

            Inform inform = new Inform();
            inform.setTopic("0");
            inform.setDesciption("Cancel Reservation Home");
            inform.setStatus_inform(1);
            inform.setDate_submit(date_submit);
            inform.setHome(home);
            inform.setUser(user);
            informRepository.saveAndFlush(inform);
            return new ResponseEntity<String>(headers,HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            headers.add("StatusValidate", "1");
            return new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-10-1
    @Update Date  -
    * method : updateHomeRent(String json)
    * Manage : Map Key ที่ Post มาแล้วทำการ Set ค่า
    */
    @RequestMapping(value = "/updateHomeRent", method = RequestMethod.POST, produces = "text/html;charset=utf-8", headers = "Accept=application/json")
    public ResponseEntity<String> updateHomeRent(@RequestBody String json) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        try {
            JSONObject jsonObject = new JSONObject(json);
            String id_books = jsonObject.getString("id_books");
            String today = jsonObject.getString("today");
            Date date_submit = new SimpleDateFormat("yyyy-MM-dd").parse(today);

            String id_user = jsonObject.getString("id_user");
            User user = profileService.getEditProfile(id_user);

            Books books = booksService.UpdateBooks(id_books);
            books.setStatus_book(3);
            Home home = books.getHome();
            home.setStatus_home(1);
            homeRepository.saveAndFlush(home);

            Contract contract = books.getContract();
            contract.setStatus_contract(0);
            contractRepository.saveAndFlush(contract);

            Inform inform = new Inform();
            inform.setTopic("3");
            inform.setDesciption("Cancel Rent Home");
            inform.setStatus_inform(1);
            inform.setDate_submit(date_submit);
            inform.setHome(home);
            inform.setUser(user);
            informRepository.saveAndFlush(inform);
            return new ResponseEntity<String>(headers,HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            headers.add("StatusValidate", "1");
            return new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
   /*
    @author  Natthakit Poltirach
    @Create Date  2019-10-1
    @Update Date  -
    * method : getInform()
    * Manage : Get ข้อมูลจาก Service
    */
    @GetMapping("/getInform")
    public List<Inform> getInform() {
        return detailHomeService.getInform();
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-10-7
    @Update Date  -
    * method : getGalleryHome
    * Manage : Get ข้อมูลจาก Service
    */
    @GetMapping("/getGalleryHome")
    public ResponseEntity<String> getGalleryHome(){
        HttpHeaders headers = new HttpHeaders();
        headers.add("statusValidate", "-1");
        try {
            return new ResponseEntity<String>((new JSONSerializer().exclude("*.class").deepSerialize(detailHomeService.getGalleryHome())), headers, HttpStatus.OK);
        } catch (Exception e) {
            headers.add("statusValidate","1");
            return new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}


