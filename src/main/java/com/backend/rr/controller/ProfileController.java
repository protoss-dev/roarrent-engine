package com.backend.rr.controller;

import com.backend.rr.entity.HomeAddress;
import com.backend.rr.entity.User;
import com.backend.rr.entity.User_Address;
import com.backend.rr.repository.UserAddressRepository;
import com.backend.rr.repository.UserProfileRepository;
import com.backend.rr.service.DetailHomeService;
import com.backend.rr.service.ProfileService;
import com.fasterxml.jackson.databind.JsonSerializable;
import flexjson.JSONSerializer;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RestController
public class ProfileController {
    @Autowired
    ProfileService profileService;
    @Autowired
    UserAddressRepository userAddressRepository;
    @Autowired
    UserProfileRepository user_profile_repository;
    /*
    @author Natthakit Poltirach
    @Create Date  2019-09-16
    @Update Date  -
    * method : editProfile
    * Manage : Get ข้อมูลจาก Backend
    */
    @RequestMapping(value = "/editProfile", method = RequestMethod.GET, produces = "text/html;charset=utf-8", headers = "Accept=application/json")
    public ResponseEntity<String> editProfile(@RequestParam(name = "id") String id_user) {
        User user = profileService.getEditProfile(id_user);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        try {
            return new ResponseEntity<String>((new JSONSerializer().exclude("*.class").deepSerialize(user)), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            headers.add("StatusValidate", "1");
            return new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @RequestMapping(value = "/updateProfile", method = RequestMethod.POST, produces = "text/html;charset=utf-8", headers = "Accept=application/json")
    public ResponseEntity<String> updateProfile(@RequestBody String json) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        try {
            JSONObject jsonObject = new JSONObject(json);
            String idUser = jsonObject.getString("id_edit_user");
            String fullName = jsonObject.getString("full_name");
            String birth = jsonObject.getString("birthday");
            Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(birth);
            String email = jsonObject.getString("email");//getString คือ key
            String phone = jsonObject.getString("phone");
            String age = jsonObject.getString("age");
            int ages = Integer.valueOf(age);
            String lineId = "";
            String imgUser = "";
            String name_home = jsonObject.getString("name_home");
            String number_home = jsonObject.getString("number_home");
            String village_no = jsonObject.getString("village_no");
            String village = jsonObject.getString("village");
            String alley = jsonObject.getString("alley");
            String road = jsonObject.getString("road");
            String district = jsonObject.getString("district");
            String prefecture = jsonObject.getString("prefecture");
            String county = jsonObject.getString("county");
            String post = jsonObject.getString("post");

            User user = profileService.getEditProfile(idUser);
            User_Address user_address = user.getUser_address();
            // set data User address
            user_address.setName_home(name_home);
            user_address.setNumber_home(number_home);
            user_address.setVillage_no(village_no);
            user_address.setVillage(village);
            user_address.setAlley(alley);
            user_address.setRoad(road);
            user_address.setDistrict(district);
            user_address.setPrefecture(prefecture);
            user_address.setCounty(county);
            user_address.setPost(post);
            userAddressRepository.saveAndFlush(user_address);

            // set data User
            user.setAge(ages);
            user.setBirthday(date1);
            user.setFullName(fullName);
            user.setEmail(email);
            user.setPhone(phone);
            user.setLine_id(lineId);
            user.setImg_user(imgUser);
            user.setUser_address(user_address);
            user_profile_repository.saveAndFlush(user);
            return new ResponseEntity<String>(headers,HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            headers.add("StatusValidate", "1");
            return new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
