package com.backend.rr.controller;

import com.backend.rr.entity.*;
import com.backend.rr.repository.*;
import com.backend.rr.service.HomeService;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.util.JSONPObject;
import flexjson.JSONSerializer;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.Array;
import java.text.SimpleDateFormat;
import java.util.*;


/*
@author  Sarawut Promsoon
@Create Date  2019-09-10
@Update Date  2019-10-2
* class : HomeController
* Manage house information
*/
@Controller
@RestController
public class HomeController {
    static Logger LOGGER = LoggerFactory.getLogger(HomeController.class);
    @Autowired
    HomeService homeService;

    @Autowired
    HomeAddressRepository homeAddressRepository;

    @Autowired
    HomeRepository homeRepository;

    @Autowired
    BankAccountRepository bankAccountRepository;

    @Autowired
    ImgHomeRepository imgHomeRepository;

    @Autowired
    InformRepository informRepository;

    @Autowired
    ContractRepository contractRepository;

    @Autowired
    BookRepository bookRepository;


    /*
    @author  Sarawut Promsoon
    @Create Date  2019-09-11
    @Update Date  2019-09-12
    * method : getAddressHome
    * Manage : Retrieve house information from the database
    */
    @GetMapping("/getAddressHome")
    public List<HomeAddress> getAddressHome(){
        return homeService.getAddressHome();
    }

    /*
    @author  Sarawut Promsoon
    @Create Date  2019-09-10
    @Update Date  2019-09-11
    * method : addHome
    * Manage : add house information
    */
    @RequestMapping(value = "/add" , method = RequestMethod.POST,produces = "text/html;charset=utf-8",headers = "Accept=application/json")
    public ResponseEntity<String> addHome(@RequestBody String json){
        System.out.println(json);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type","application/json; charset=utf-8");
        try{
            JSONObject jsonObject = new JSONObject(json);
            String home_name = jsonObject.getString("home_name");
            String home_number = jsonObject.getString("home_number");
            String village_no = jsonObject.getString("village_no");
            String village = jsonObject.getString("village");
            String alley = jsonObject.getString("alley");
            String road = jsonObject.getString("road");
            String district = jsonObject.getString("district");
            String district_area = jsonObject.getString("district_area");
            String county = jsonObject.getString("county");
            String zipCode = jsonObject.getString("zipCode");

            String house_sizeS = jsonObject.getString("sizeHome");
            String room_sizeS = jsonObject.getString("sizeRoom");
            String number_floorsS = jsonObject.getString("number_floors");
            String number_roomS = jsonObject.getString("number_room");
            String number_toiletS = jsonObject.getString("number_toilet");
            String cost_waterS = jsonObject.getString("water");
            String cost_fireS = jsonObject.getString("electricity");
            String cost_hireS = jsonObject.getString("month");
            String imgName = jsonObject.getString("img");
            int sizeImg = jsonObject.getInt("sizeImg");
            int house_size = Integer.valueOf(house_sizeS);
            int room_size = Integer.valueOf(room_sizeS);
            int number_floors = Integer.valueOf(number_floorsS);
            int number_room = Integer.valueOf(number_roomS);
            int number_toilet = Integer.valueOf(number_toiletS);
            int cost_water = Integer.valueOf(cost_waterS);
            int cost_fire = Integer.valueOf(cost_fireS);
            int cost_hire = Integer.valueOf(cost_hireS);
            String size = jsonObject.getString("size_bank");

            int size_bank = Integer.valueOf(size);
            JSONArray bank = jsonObject.getJSONArray("bank");

            Home home = new Home();
            HomeAddress homeAddress = new HomeAddress();
            User user = homeService.getUser("1");

            // set data address home
            homeAddress.setNameHome(home_name);
            homeAddress.setNumberHome(home_number);
            homeAddress.setVillageNo(village_no);
            homeAddress.setVillage(village);
            homeAddress.setAlley(alley);
            homeAddress.setRoad(road);
            homeAddress.setDistrict(district);
            homeAddress.setPrefecture(district_area);
            homeAddress.setCounty(county);
            homeAddress.setPost(zipCode);
            homeAddressRepository.saveAndFlush(homeAddress);

            // set data home
            home.setHouse_size(house_size);
            home.setRoom_size(room_size);
            home.setNumber_floors(number_floors);
            home.setNumber_room(number_room);
            home.setNumber_toilet(number_toilet);
            home.setCost_water(cost_water);
            home.setCost_fire(cost_fire);
            home.setCost_hire(cost_hire);
            home.setHomeAddress(homeAddress);
            home.setStatus_home(1);
            home.setUser(user);
            homeRepository.saveAndFlush(home);

            Set<Bank_Account> setBank = new HashSet<Bank_Account>();
            //  set data bank
            for(int i = 0 ; i < size_bank ;i++){
                Bank_Account bank_account = new Bank_Account();
                JSONObject bank_index = bank.getJSONObject(i);
                String name_bank = bank_index.getString("bank");
                String number_bank = bank_index.getString("number_bank");

                bank_account.setName_bank(name_bank);
                bank_account.setNumber_account(number_bank);
                bank_account.setHome(home);
                bankAccountRepository.saveAndFlush(bank_account);

                setBank.add(bank_account);
            }
            home.setBank_account(setBank);
            homeRepository.saveAndFlush(home);

            Set<Img_Home> setImg = new HashSet<Img_Home>();
            for(int i = 1 ; i <= sizeImg ;i++){
                Img_Home img_home = new Img_Home();
                img_home.setImg_home(imgName+"H"+home_name+"_"+i);
                img_home.setHome(home);
                imgHomeRepository.saveAndFlush(img_home);
                setImg.add(img_home);
            }
            home.setImg_home(setImg);
            homeRepository.saveAndFlush(home);
            return new ResponseEntity<String>(headers,HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            headers.add("statusValidate","1");
            return new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    /*
    @author  Sarawut Promsoon
    @Create Date  2019-09-11
    @Update Date  2019-09-12
    * method : getHome
    * Manage : Retrieve house information from the database
    */
    @GetMapping("/getHome")
    public ResponseEntity<String> getHome(){
        HttpHeaders headers = new HttpHeaders();
        headers.add("statusValidate", "-1");
        try {
        return new ResponseEntity<String>((new JSONSerializer().exclude("*.class").deepSerialize(homeService.getHome())), headers, HttpStatus.OK);
        } catch (Exception e) {
            headers.add("statusValidate","1");
            return new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /*
    @author  Sarawut Promsoon
    @Create Date  2019-09-11
    @Update Date  -
    * method : getEditHome
    * Manage : Retrieve house information from the database
    */
    @RequestMapping(value = "/getEditHome" , method = RequestMethod.GET,produces = "text/html;charset=utf-8",headers = "Accept=application/json")
    public ResponseEntity<String> getEditHome(@RequestParam(name = "id") String id_home) {
        System.out.println("controller id : " + id_home);
        Home home = homeService.getEditHome(id_home);
        HttpHeaders headers = new HttpHeaders();
        headers.add("statusValidate", "-1");

        try {
            return new ResponseEntity<String>((new JSONSerializer().exclude("*.class").deepSerialize(home)), headers, HttpStatus.OK);
        } catch (Exception e) {
            headers.add("statusValidate","1");
            return new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
    @author  Sarawut Promsoon
    @Create Date  2019-09-11
    @Update Date  2019-09-17
    * method : updateHome
    * Manage : edit house information
    */
    @RequestMapping(value = "/updateHome" , method = RequestMethod.POST,produces = "text/html;charset=utf-8",headers = "Accept=application/json")
    public ResponseEntity<String> updateHome(@RequestBody String json){
        System.out.println(json);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type","application/json; charset=utf-8");
        try{
            JSONObject jsonObject = new JSONObject(json);
            String id_home = jsonObject.getString("id_home");
            String home_name = jsonObject.getString("home_name");
            String home_number = jsonObject.getString("home_number");
            String village_no = jsonObject.getString("village_no");
            String village = jsonObject.getString("village");
            String alley = jsonObject.getString("alley");
            String road = jsonObject.getString("road");
            String district = jsonObject.getString("district");
            String district_area = jsonObject.getString("district_area");
            String county = jsonObject.getString("county");
            String zipCode = jsonObject.getString("zipCode");

            String house_sizeS = jsonObject.getString("sizeHome");
            String room_sizeS = jsonObject.getString("sizeRoom");
            String number_floorsS = jsonObject.getString("number_floors");
            String number_roomS = jsonObject.getString("number_room");
            String number_toiletS = jsonObject.getString("number_toilet");
            String cost_waterS = jsonObject.getString("water");
            String cost_fireS = jsonObject.getString("electricity");
            String cost_hireS = jsonObject.getString("month");

//            String id_bank = jsonObject.getString("id_bank");
            String sizeB = jsonObject.getString("size_bank");
//            int sizeBankArr = jsonObject.getInt("idBankArr");
            String imgName = jsonObject.getString("img");
            int sizeImg = jsonObject.getInt("sizeImg");
            int sizeStart = jsonObject.getInt("sizeStart");
            int house_size = Integer.valueOf(house_sizeS);
            int room_size = Integer.valueOf(room_sizeS);
            int number_floors = Integer.valueOf(number_floorsS);
            int number_room = Integer.valueOf(number_roomS);
            int number_toilet = Integer.valueOf(number_toiletS);
            int cost_water = Integer.valueOf(cost_waterS);
            int cost_fire = Integer.valueOf(cost_fireS);
            int cost_hire = Integer.valueOf(cost_hireS);

            int size_bank = Integer.valueOf(sizeB);
            JSONArray bank = jsonObject.getJSONArray("bank");

            Home home = homeService.getEditHome(id_home);
            HomeAddress homeAddress = home.getHomeAddress();

            //  set data bank
            Set<Bank_Account> setBank = new HashSet<Bank_Account>();
            for(int i = 0 ; i < size_bank ;i++){
                JSONObject bank_index = bank.getJSONObject(i);
                String check = bank_index.getString("id_bank");
                String name_bank = bank_index.getString("bank");
                String number_bank = bank_index.getString("number_bank");
                if(!check.equals("no")){
                    Bank_Account bank_account = homeService.getBank(check);
                    bank_account.setName_bank(name_bank);
                    bank_account.setNumber_account(number_bank);
                    bank_account.setHome(home);
                    bankAccountRepository.saveAndFlush(bank_account);
                    setBank.add(bank_account);
                }else{
                    Bank_Account addBank_account = new Bank_Account();
                    addBank_account.setName_bank(name_bank);
                    addBank_account.setNumber_account(number_bank);
                    addBank_account.setHome(home);
                    bankAccountRepository.saveAndFlush(addBank_account);
                    setBank.add(addBank_account);
                }
            }
            //  set data bank
            Set<Img_Home> setImg = new HashSet<Img_Home>();
            for(int i = 1 ; i <= sizeImg ;i++){
                if(i > sizeStart){
                    Img_Home img_home = new Img_Home();
                    img_home.setImg_home(imgName+"H"+home_name+"_"+i);
                    img_home.setHome(home);
                    imgHomeRepository.saveAndFlush(img_home);
                    setImg.add(img_home);
                }
            }


            // set data address home
            homeAddress.setNameHome(home_name);
            homeAddress.setNumberHome(home_number);
            homeAddress.setVillageNo(village_no);
            homeAddress.setVillage(village);
            homeAddress.setAlley(alley);
            homeAddress.setRoad(road);
            homeAddress.setDistrict(district);
            homeAddress.setPrefecture(district_area);
            homeAddress.setCounty(county);
            homeAddress.setPost(zipCode);
            homeAddressRepository.saveAndFlush(homeAddress);
            // set data home
            home.setHouse_size(house_size);
            home.setRoom_size(room_size);
            home.setNumber_floors(number_floors);
            home.setNumber_room(number_room);
            home.setNumber_toilet(number_toilet);
            home.setCost_water(cost_water);
            home.setCost_fire(cost_fire);
            home.setCost_hire(cost_hire);
            home.setHomeAddress(homeAddress);
            home.setBank_account(setBank);
            home.setImg_home(setImg);
            homeRepository.saveAndFlush(home);
            System.out.println("save : รอ");
            return new ResponseEntity<String>(headers,HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            headers.add("statusValidate","1");
            return new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /*
    @author  Sarawut Promsoon
    @Create Date  2019-09-19
    @Update Date
    * method : getNotify
    * Manage : Retrieve Notify information from the database
    */
    @GetMapping("/getNotify")
    public ResponseEntity<String> getNotify(){
        HttpHeaders headers = new HttpHeaders();
        headers.add("statusValidate", "-1");
        try {
            return new ResponseEntity<String>((new JSONSerializer().exclude("*.class").deepSerialize(homeService.getNotify())), headers, HttpStatus.OK);
        } catch (Exception e) {
            headers.add("statusValidate","1");
            return new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
    @author  Sarawut Promsoon
    @Create Date  2019-09-10
    @Update Date  2019-09-11
    * method : updateStatusNotify
    * Manage : edit Status Notify
    */
    @RequestMapping(value = "/updateStatusNotify" , method = RequestMethod.POST,produces = "text/html;charset=utf-8",headers = "Accept=application/json")
    public ResponseEntity<String> updateStatusNotify(@RequestBody String json){
        System.out.println(json);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type","application/json; charset=utf-8");
        try{
            JSONObject jsonObject = new JSONObject(json);
            String id_inform = jsonObject.getString("id_inform");
            String status = jsonObject.getString("status");
            int status_inform = Integer.valueOf(status);

            Inform inform = homeService.getEditStatusInform(id_inform);
            inform.setStatus_inform(status_inform);
            informRepository.saveAndFlush(inform);

            return new ResponseEntity<String>(headers,HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            headers.add("statusValidate","1");
            return new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
    @author  Sarawut Promsoon
    @Create Date  2019-09-20
    @Update Date
    * method : getDataBook
    * Manage : Retrieve house information from the database
    */
    @RequestMapping(value = "/getDataBook" , method = RequestMethod.GET,produces = "text/html;charset=utf-8",headers = "Accept=application/json")
    public ResponseEntity<String> getDataBook() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("statusValidate", "-1");

        try {
            return new ResponseEntity<String>((new JSONSerializer().exclude("*.class").deepSerialize(homeService.getDataBookAll())), headers, HttpStatus.OK);
        } catch (Exception e) {
            headers.add("statusValidate","1");
            return new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
    @author  Sarawut Promsoon
    @Create Date  2019-09-20
    @Update Date
    * method : deleteHome
    * Manage :
    */
    @RequestMapping(value = "/deleteHome" , method = RequestMethod.POST,produces = "text/html;charset=utf-8",headers = "Accept=application/json")
    public ResponseEntity<String> deleteHome(@RequestBody String json){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type","application/json; charset=utf-8");

        try{
            JSONObject jsonObject = new JSONObject(json);
            String id_home = jsonObject.getString("id");
            System.out.println(id_home);
            int staus = 0;
            Home home = homeService.getEditHome(id_home);
            home.setStatus_home(staus);
            homeRepository.saveAndFlush(home);

            return new ResponseEntity<String>(headers,HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            headers.add("statusValidate","1");
            return new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
    @author  Sarawut Promsoon
    @Create Date  2019-09-23
    @Update Date
    * method : saveContract
    * Manage : add Contract information
    */
    @RequestMapping(value = "/saveContract" , method = RequestMethod.POST,produces = "text/html;charset=utf-8",headers = "Accept=application/json")
    public ResponseEntity<String> saveContract(@RequestBody String json){
        System.out.println(json);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type","application/json; charset=utf-8");
        try{
            JSONObject jsonObject = new JSONObject(json);
            String id_home = jsonObject.getString("id_home");
            String id_book = jsonObject.getString("id_book");
            String dateStart = jsonObject.getString("dateStart");
            Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(dateStart);

            String dateEnd = jsonObject.getString("dateEnd");
            Date date2 = new SimpleDateFormat("yyyy-MM-dd").parse(dateEnd);

            String length = jsonObject.getString("lengthDate");
            int lengthDate = Integer.parseInt(length);
            String rentCost = jsonObject.getString("rent");
            int rent = Integer.parseInt(rentCost);
            String bail = jsonObject.getString("bail");
            String namePdf = jsonObject.getString("namePdf");
            String name_ReceiptPdf = jsonObject.getString("name_ReceiptPdf");
            System.out.println("=================== "+name_ReceiptPdf);
            Home home = homeService.getEditHome(id_home);
            Contract contract = new Contract();
            Books books = homeService.getDataBook(id_book);

            contract.setDate_start(date1);
            contract.setDate_end(date2);
            contract.setPhase_time(lengthDate);
            contract.setCost_hire(rent);
            contract.setBail(bail);
            contract.setContract_pdf(namePdf);
            contract.setInvoive_pdf(name_ReceiptPdf);
            contract.setHome(home);
            contract.setStatus_contract(1);

            contractRepository.saveAndFlush(contract);
            books.setStatus_book(2);
            books.setContract(contract);
            bookRepository.saveAndFlush(books);

            home.setStatus_home(3);
            homeRepository.saveAndFlush(home);

            return new ResponseEntity<String>(headers,HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            headers.add("statusValidate","1");
            return new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
    @author  Sarawut Promsoon
    @Create Date  2019-09-19
    @Update Date
    * method : pdfContract
    * Manage :
    */
     @RequestMapping(value = "/pdfContract" , method = RequestMethod.POST,produces = "text/html;charset=utf-8",headers = "Accept=application/json")
     public ResponseEntity<String> pdfContract(@RequestBody String json){
        HttpHeaders headers = new HttpHeaders();
        headers.add("statusValidate", "-1");
        try {
            return new ResponseEntity<String>((new JSONSerializer().exclude("*.class").deepSerialize(homeService.getNotify())), headers, HttpStatus.OK);
        } catch (Exception e) {
            headers.add("statusValidate","1");
            return new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
    @author  Sarawut Promsoon
    @Create Date  2019-09-20
    @Update Date
    * method : deleteHome
    * Manage :
    */
    @RequestMapping(value = "/deleteContract" , method = RequestMethod.POST,produces = "text/html;charset=utf-8",headers = "Accept=application/json")
    public ResponseEntity<String> deleteContract(@RequestBody String json){
        System.out.println(json);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type","application/json; charset=utf-8");
        try{
            JSONObject jsonObject = new JSONObject(json);
            String id_home = jsonObject.getString("id_home");
            String id_book = jsonObject.getString("id_book");
            String id_contract = jsonObject.getString("id_contract");
            long idcontract = Long.parseLong(id_contract);
            long idbook= Long.parseLong(id_book);

            Home home = homeService.getEditHome(id_home);
            Books books = homeService.getDataBook(id_book);
            Contract contract = homeService.getContract(id_contract);

            home.setStatus_home(1);
            homeRepository.saveAndFlush(home);

            books.setStatus_book(0);
            bookRepository.saveAndFlush(books);

            contract.setStatus_contract(0);
            contractRepository.saveAndFlush(contract);

            return new ResponseEntity<String>(headers,HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            headers.add("statusValidate","1");
            return new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
    @author  Sarawut Promsoon
    @Create Date  2019-09-11
    @Update Date  2019-09-12
    * method : getContract
    * Manage : Retrieve Contract information from the database
    */
    @GetMapping("/getContract")
    public ResponseEntity<String> getContract(){
        HttpHeaders headers = new HttpHeaders();
        headers.add("statusValidate", "-1");
        try {
            return new ResponseEntity<String>((new JSONSerializer().exclude("*.class").deepSerialize(homeService.getContractAll())), headers, HttpStatus.OK);
        } catch (Exception e) {
            headers.add("statusValidate","1");
            return new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /*
     @author  Sarawut Promsoon
     @Create Date  2019-09-11
     @Update Date  -
     * method : getDataUser
     * Manage : Retrieve User information from the database
     */
    @GetMapping("/getDataUser")
    public ResponseEntity<String> getDataUser(){
        HttpHeaders headers = new HttpHeaders();
        headers.add("statusValidate", "-1");
        try {
            return new ResponseEntity<String>((new JSONSerializer().exclude("*.class").deepSerialize(homeService.getUser("1"))), headers, HttpStatus.OK);
        } catch (Exception e) {
            headers.add("statusValidate","1");
            return new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
     @author  Sarawut Promsoon
     @Create Date 2019-10-02
     @Update Date -
     * method : getDataBank
     * Manage :
     */
    @GetMapping("/getDataBank")
    public ResponseEntity<String> getDataBank(){
        HttpHeaders headers = new HttpHeaders();
        headers.add("statusValidate", "-1");
        try {
            return new ResponseEntity<String>((new JSONSerializer().exclude("*.class").deepSerialize(homeService.getDataBank())), headers, HttpStatus.OK);
        } catch (Exception e) {
            headers.add("statusValidate","1");
            return new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
     @author  Sarawut Promsoon
     @Create Date  2019-10-02
     @Update Date
     * method : getDataBank
     * Manage :
     */
    @GetMapping("/getDataImage")
    public ResponseEntity<String> getDataImage(){
        HttpHeaders headers = new HttpHeaders();
        headers.add("statusValidate", "-1");
        try {
            return new ResponseEntity<String>((new JSONSerializer().exclude("*.class").deepSerialize(homeService.getDataImage())), headers, HttpStatus.OK);
        } catch (Exception e) {
            headers.add("statusValidate","1");
            return new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
