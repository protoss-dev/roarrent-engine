package com.backend.rr.repository;

import com.backend.rr.entity.Books;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

/*
@author  Natthakit Poltirach
@Create Date  2019-09-23
@Update Date  -
* interface : BooksRepository
* Manage :
*/
public interface BooksRepository extends JpaSpecificationExecutor<Books>, JpaRepository<Books,Long>, PagingAndSortingRepository<Books,Long> {
}
