package com.backend.rr.repository;

import com.backend.rr.entity.Home;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

/*
@author  Sarawut Promsoon
@Create Date  2019-09-10
@Update Date  -
* interface : HomeRepository
* Manage :
*/
public interface HomeRepository extends JpaSpecificationExecutor<Home>, JpaRepository<Home,Long>, PagingAndSortingRepository<Home,Long>
{
}
