package com.backend.rr.repository;

import com.backend.rr.entity.User_Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
/*
@author Natthakit Poltirach
@Create Date  2019-09-16
@Update Date  -
* interface : UserAddressRepository
* Manage :
*/
public interface UserAddressRepository extends JpaSpecificationExecutor<User_Address>, JpaRepository<User_Address,Long>, PagingAndSortingRepository<User_Address,Long> {
}
