package com.backend.rr.repository;

import com.backend.rr.entity.Img_Home;
import com.backend.rr.entity.Inform;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
/*
@author  Natthakit Poltirach
@Create Date  2019-09-25
@Update Date  -
* interface : InformRepository
* Manage :
*/
public interface InformRepository extends JpaSpecificationExecutor<Inform>, JpaRepository<Inform,Long>, PagingAndSortingRepository<Inform,Long>
{
}