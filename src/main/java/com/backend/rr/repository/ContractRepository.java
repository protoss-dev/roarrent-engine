package com.backend.rr.repository;

import com.backend.rr.entity.Contract;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ContractRepository extends JpaSpecificationExecutor<Contract>, JpaRepository<Contract,Long>, PagingAndSortingRepository<Contract,Long> {
}
