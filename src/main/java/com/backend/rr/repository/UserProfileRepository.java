package com.backend.rr.repository;

import com.backend.rr.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

/*
@author Natthakit Poltirach
@Create Date  2019-09-11
@Update Date  2019-09-18
* interface : UserProfileRepository
* Manage :
*/
public interface UserProfileRepository extends JpaSpecificationExecutor<User>, JpaRepository<User,Long>, PagingAndSortingRepository<User,Long> {
}