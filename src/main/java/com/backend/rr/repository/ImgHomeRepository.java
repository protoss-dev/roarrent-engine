package com.backend.rr.repository;

import com.backend.rr.entity.Img_Home;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

/*
@author  Sarawut Promsoon
@Create Date  2019-09-10
@Update Date  -
* interface : ImgHomeRepository
* Manage :
*/
public interface ImgHomeRepository extends JpaSpecificationExecutor<Img_Home>, JpaRepository<Img_Home,Long>, PagingAndSortingRepository<Img_Home,Long>
{
}
