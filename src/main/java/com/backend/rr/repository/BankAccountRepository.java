package com.backend.rr.repository;

import com.backend.rr.entity.Bank_Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

/*
@author  Sarawut Promsoon
@Create Date  2019-09-10
@Update Date  -
* interface : BankAccountRepository
* Manage :
*/
public interface BankAccountRepository extends JpaSpecificationExecutor<Bank_Account>, JpaRepository<Bank_Account,Long>, PagingAndSortingRepository<Bank_Account,Long>
{
}