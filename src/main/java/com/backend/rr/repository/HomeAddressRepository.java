package com.backend.rr.repository;

import com.backend.rr.entity.HomeAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

/*
@author  Sarawut Promsoon
@Create Date  2019-09-10
@Update Date  -
* interface : HomeAddressRepository
* Manage :
*/
public interface HomeAddressRepository extends JpaSpecificationExecutor<HomeAddress>, JpaRepository<HomeAddress,Long>, PagingAndSortingRepository<HomeAddress,Long>
{


}
