package com.backend.rr.service;

import com.backend.rr.entity.Home;
import com.backend.rr.entity.Img_Home;
import com.backend.rr.entity.Inform;
import com.backend.rr.repository.HomeRepository;
import com.backend.rr.repository.ImgHomeRepository;
import com.backend.rr.repository.InformRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/*
@author  Sarawut Promsoon
@Create Date  2019-09-11
@Update Date  2019-09-18
* class : DetailHomeServiceImp
* Manage : implements interface และ ทำการ Query ข้อมูล
*/
@Service
public class DetailHomeServiceImp implements DetailHomeService {

    @Autowired
    HomeRepository homeRepository;

    @Autowired
    InformRepository informRepository;

    @Autowired
    ImgHomeRepository imgHomeRepository;

    @Override
    public List<Home> getDetailHome() {
        return homeRepository.findAll();

    }

    @Override
    public Home getReservDetailHome(String id_home){
        long id = Long.parseLong(id_home);
        return homeRepository.getOne(id);
    }

    @Override
    public List<Inform> getInform() {
        return informRepository.findAll();
    }

    @Override
    public List<Img_Home> getGalleryHome() {
        return imgHomeRepository.findAll();
    }
}
