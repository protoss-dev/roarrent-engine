package com.backend.rr.service;

import com.backend.rr.entity.Books;
import java.awt.print.Book;
import java.util.List;
/*
@author  Natthakit Poltirach
@Create Date  2019-09-23
@Update Date  -
* Interface : BooksService
* Manage : get ข้อมูลจาก Entity
*/
public interface BooksService {
    List<Books> getListBooks();

    Books UpdateBooks(String id_books);
}
