package com.backend.rr.service;

import com.backend.rr.entity.Home;
import com.backend.rr.entity.Img_Home;
import com.backend.rr.entity.Inform;

import java.util.List;

/*
@author  Natthakit Poltirach
@Create Date  2019-09-16
@Update Date  2019-09-18
* Interface : DetailHomeService
* Manage : get ข้อมูลจาก Entity
*/
public interface DetailHomeService {
    List<Home>getDetailHome();

    Home getReservDetailHome(String id_home);

    List<Inform>getInform();

    List<Img_Home>getGalleryHome();
}

