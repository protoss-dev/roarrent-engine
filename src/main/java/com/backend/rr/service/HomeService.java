package com.backend.rr.service;

import com.backend.rr.entity.*;
import org.hibernate.mapping.Map;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/*
@author  Sarawut Promsoon
@Create Date  2019-09-11
@Update Date  -
* interface : HomeService
* set method required
*/
public interface HomeService {
    List<HomeAddress> getAddressHome();
    List<Home> getHome();
    Home getEditHome(String id_home);

    List<Inform> getNotify();

    Inform getEditStatusInform(String id_inform);
    User getUser(String id_user);

    Books getDataBook(String id_book);
    List<Books> getDataBookAll();
    Bank_Account getBank(String id_bank);

    Contract getContract(String id_Contract);
    List<Contract> getContractAll();

    List<Bank_Account> getDataBank();
    List<Img_Home> getDataImage();

}
