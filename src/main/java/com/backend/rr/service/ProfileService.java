package com.backend.rr.service;

import com.backend.rr.entity.User;

/*
@author  Natthakit Poltirach
@Create Date  2019-09-17
@Update Date  -
* Interface : ProfileService
* Manage : get ข้อมูลจาก Entity
*/
public interface ProfileService {//ใส่ใน Controller
    User getEditProfile(String id_user);
}
