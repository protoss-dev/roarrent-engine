package com.backend.rr.service;

import com.backend.rr.entity.*;
import com.backend.rr.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/*
@author  Sarawut Promsoon
@Create Date  2019-09-11
@Update Date  2019-10-2
* class : HomeServiceImp
* set doing work method required
*/
@Service
public class HomeServiceImp implements HomeService {

    @Autowired
    HomeAddressRepository homeAddressRepository;

    @Autowired
    InformRepository informRepository;

    @Autowired
    UserProfileRepository userRepository;

    @Autowired
    BookRepository bookRepository;

    @Autowired
    BankAccountRepository bankAccountRepository;

    @Autowired
    ContractRepository contractRepository;

    @Autowired
    ImgHomeRepository imgHomeRepository;


    @Override
    public List<HomeAddress> getAddressHome() {
        return homeAddressRepository.findAll();
    }


    @Autowired
    HomeRepository homeRepository;


    @Override
    public List<Home> getHome() {
        return homeRepository.findAll();
    }

    @Override
    public Home getEditHome(String id_home){
        long id_homeEdit = Long.parseLong(id_home);
        return homeRepository.getOne(id_homeEdit);
    }

    @Override
    public List<Inform> getNotify(){
        return informRepository.findAll();
    }

    @Override
    public Inform getEditStatusInform(String id_inform){
        long id_informEdit = Long.parseLong(id_inform);
        return informRepository.getOne(id_informEdit);
    }

    @Override
    public User getUser(String user){
        long id_user = Long.parseLong(user);
        return userRepository.getOne(id_user);
    }

    @Override
    public Books getDataBook(String id_book){
        System.out.println("service : "+id_book);
        long id = Long.parseLong(id_book);
        return bookRepository.getOne(id);
    }

    @Override
    public Bank_Account getBank(String id_bank){
        long id_bankEdit = Long.parseLong(id_bank);
        return bankAccountRepository.getOne(id_bankEdit);
    }

    @Override
    public Contract getContract(String id_Contract){
        long id = Long.parseLong(id_Contract);
        return contractRepository.getOne(id);
    }

    @Override
    public List<Contract> getContractAll(){
        return contractRepository.findAll();
    }

    @Autowired
    public List<Books> getDataBookAll(){
        return bookRepository.findAll();
    }

    @Override
    public List<Bank_Account> getDataBank(){
        return bankAccountRepository.findAll();
    }

    @Override
    public List<Img_Home> getDataImage(){
        return imgHomeRepository.findAll();
    }
}

