package com.backend.rr.service;

import com.backend.rr.entity.Books;
import com.backend.rr.repository.BooksRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/*
@author  Natthakit Poltirach
@Create Date  2019-09-23
@Update Date  -
* class : BooksServiceImp
* Manage : implements interface และ ทำการ Query ข้อมูล
*/
@Service
public class BooksServiceImp implements BooksService{

    @Autowired
    BooksRepository booksRepository;

    @Override
    public List<Books> getListBooks() {
        return booksRepository.findAll();
    }
    @Override
    public Books UpdateBooks(String id_books){
        long id = Long.parseLong(id_books);
        return booksRepository.getOne(id);
    }
}
