package com.backend.rr.service;

import com.backend.rr.entity.User;
import com.backend.rr.repository.UserProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/*
@author Natthakit Poltirach
@Create Date  2019-09-11
@Update Date  2019-09-18
* class : ProfileServiceImp
* Manage : Get ข้อมูลจาก Service
*/
@Service
public class ProfileServiceImp implements ProfileService {
    @Autowired
    UserProfileRepository userProfileRepository;

    @Override
    public User getEditProfile(String id_user){
        long id = Long.parseLong(id_user);
        return userProfileRepository.getOne(id);
    }
}
