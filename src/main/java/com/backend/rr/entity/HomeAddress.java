package com.backend.rr.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class HomeAddress {
    private @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) Long id_address ;
    private String numberHome;
    private String villageNo;
    private String village;
    private String alley;
    private String road;
    private String district;
    private String prefecture;
    private String county;
    private String post;
    private String nameHome;

    @OneToOne
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Home home;
}
