package com.backend.rr.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class Books {
    private @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) Long id_book;
    private Date date_book;
    private String bill_book;
    private int status_book;

    @OneToOne(cascade = CascadeType.ALL)
    private Home home;

    @OneToOne(cascade = CascadeType.ALL)
    private User user;

    @OneToOne(cascade = CascadeType.ALL)
    private Contract contract;
}
