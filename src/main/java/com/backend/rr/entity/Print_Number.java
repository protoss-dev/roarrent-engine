package com.backend.rr.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Print_Number {
    private @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) Long id_print_number;
    private int contract_number;
    private int invoive_number;

    @OneToOne(cascade = CascadeType.ALL)
    private Contract contract;
}
