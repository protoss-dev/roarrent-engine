package com.backend.rr.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Reviews {
    private @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) Long id_reviews;
    private Date date_reviews;
    private int score_reviews;
    private int text_reviews;
}
