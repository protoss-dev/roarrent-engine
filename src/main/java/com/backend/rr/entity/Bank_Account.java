package com.backend.rr.entity;



import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class Bank_Account {
    private @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) Long id_bank;
    private String name_bank;
    private String number_account;
    private int status_bank;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "home")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Home home;
}
