package com.backend.rr.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class Inform {
    private @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) Long id_inform;
    private String topic;
    private String desciption;
    private Date date_submit;
    private int status_inform;

    @OneToOne(cascade = CascadeType.ALL)
    private Home home;

    @OneToOne(cascade = CascadeType.ALL)
    private User user;
}
