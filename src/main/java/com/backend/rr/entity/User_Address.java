package com.backend.rr.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class User_Address {
    private @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) Long id_address;
    private String name_home;
    private String number_home;
    private String village_no;
    private String village;
    private String alley;
    private String road;
    private String district;
    private String prefecture;
    private String county;
    private String post;

    @OneToOne
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private User user;
}
