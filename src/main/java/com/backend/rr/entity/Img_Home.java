package com.backend.rr.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class Img_Home {
    private @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) Long id_img_home;
    private String img_home;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "home")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Home home;

}
