package com.backend.rr.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class User {
    private @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) Long id_user;
    private String email;
    private String passwords;
    private String fullName;
    private int age;
    private Date birthday;
    private String line_id;
    private String phone;
    private String img_user;

    @OneToOne(cascade = CascadeType.ALL)
    private User_Address user_address;

    @OneToOne
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Home homes;

    @OneToOne
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Books books;

    @OneToOne
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Inform inform;

    @OneToOne
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Contract contract;

}
