package com.backend.rr.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class Contract {
    private @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) Long id_contract;
    private Date date_start;
    private Date date_end;
    private int phase_time;
    private int cost_hire;
    private int cost_water;
    private int cost_fire;
    private String bail;
    private String contract_pdf;
    private String invoive_pdf;
    private int status_contract;


    @OneToOne(cascade = CascadeType.ALL)
    private Home home;

    @OneToOne
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Print_Number print_number;

    @OneToOne
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Books books;

    @OneToOne(cascade = CascadeType.ALL)
    private User user;
}
