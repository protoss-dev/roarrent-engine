package com.backend.rr.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class Home {
    private @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) Long id_home;
    private int house_size;
    private int room_size;
    private int number_floors;
    private int number_room;
    private int number_toilet;
    private int cost_hire;
    private int cost_water;
    private int cost_fire;
    private int status_home;


    @OneToOne(cascade = CascadeType.ALL)
    private HomeAddress homeAddress;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "home")
    private Set<Bank_Account> bank_account = new HashSet<Bank_Account>();

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "home")
    private Set<Img_Home> img_home = new HashSet<Img_Home>();

    @OneToOne
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Inform inform;

    @OneToOne(cascade = CascadeType.ALL)
    private User user;

    @OneToOne
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Contract contract;

    @OneToOne
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Books books;
}
